package main

import (
	"fmt"
	log "github.com/rs/zerolog"

	"gitlab.com/spaceshiperr/cicd-demo-project/greeter"
)

func main() {
	logger := log.Logger{}

	for {
		var name string

		if _, err := fmt.Scanln(&name); err != nil {
			logger.Error().Msgf(err.Error())
		}

		message, err := greeter.Greet(name)
		if err != nil {
			logger.Error().Msgf(err.Error())
		}

		fmt.Println(message)
	}
}
