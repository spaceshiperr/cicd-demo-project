package greeter_test

import (
	"github.com/stretchr/testify/require"
	"testing"

	"gitlab.com/spaceshiperr/cicd-demo-project/greeter"
)

func TestGreet(t *testing.T) {
	expected := "Hello, tester name! Nice to meet you!"

	got, err := greeter.Greet("tester name")
	require.NoError(t, err)
	require.Equal(t, expected, got)
}
