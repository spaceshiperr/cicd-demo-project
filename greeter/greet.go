package greeter

import (
	"errors"
	"fmt"
)

func Greet(name string) (string, error) {
	if name == "" {
		return "", errors.New("empty name argument")
	}

	return fmt.Sprintf("Hello, %s! Nice to meet you!", name), nil
}
